import java.util.ArrayList;

public class Contact {
    //Properties
    private String name;
    //    private String contactNumber;
    private ArrayList<String> contactNumber;
    private ArrayList<String> address;

    //Constructors
    public Contact(){};
    public Contact(String name, String contactNumber, String address){
        this.name = name;

        this.contactNumber = new ArrayList<>();
        this.contactNumber.add(contactNumber);

        this.address = new ArrayList<>();
        this.address.add(address);
    }

    //Getter
    public String getName(){
        return this.name;
    }

    public ArrayList<String> getContactNumber(){
        return this.contactNumber;
    }

    public ArrayList<String> getAddress(){
        return this.address;
    }

    //Setter
    public void setName(String name){
        this.name = name;
    }

    public void setContactNumber(ArrayList<String> contactNumber){
        this.contactNumber = contactNumber;
    }

    public void setAddress(ArrayList<String> address){
        this.address = address;
    }

    //Methods
    public void addContactNumber(String number){
        this.contactNumber.add(number);
    }

    public void removeContactNumber(String number){
        this.contactNumber.remove(number);
    }

    public void addAddress(String address){
        this.address.add(address);
    }

    public void removeAddress(String address){
        this.address.remove(address);
    }

}
