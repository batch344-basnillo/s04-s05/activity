import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        //Horizontal Line
        int lineLength = 20;

        //Task 9
        Phonebook phonebook = new Phonebook();

        //Task 10
        Contact contact1 = new Contact("John Doe", "+639152468596", "My home in Quezon City");
        contact1.addContactNumber("+639228547963");
        contact1.addAddress("My office in Makati City");

        Contact contact2 = new Contact("Jane Doe", "+639162148573", "My home in Caloocan City");
        contact2.addContactNumber("+639173698541");
        contact2.addAddress("My office in Pasay City");

        //Task 11
        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        //Task 12
        if (phonebook.isEmpty()){
            System.out.println("The phonebook is empty");
        } else {
            ArrayList<Contact> contactsInsidePhonebook = phonebook.getContacts();
            for (Contact contact : contactsInsidePhonebook) {
                System.out.println(contact.getName());
                for (int i = 0; i < lineLength; i++) {
                    System.out.print("-");
                }
                System.out.println();
                System.out.println(contact.getName() + " has the following registered numbers:");
                for (String contactNumber : contact.getContactNumber()){
                    System.out.println(contactNumber);
                }
                for (int i = 0; i < lineLength; i++) {
                    System.out.print("-");
                }
                System.out.println();
                System.out.println(contact.getName() + " has the following registered addresses:");
                for (String address : contact.getAddress()){
                    System.out.println(address);
                }
                System.out.println();
            }
        }

    }
}