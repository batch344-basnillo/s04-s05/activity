import java.util.ArrayList;

public class Phonebook {
    //Property
    private ArrayList<Contact> contacts;
    private Contact contact;

    //Constructor
    public Phonebook(){
        this.contacts = new ArrayList<>();
    };

    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    //Getter
    public ArrayList<Contact> getContacts(){
        return this.contacts;
    }

    //Setter
    public void setContacts(Contact contact){
        this.contacts.add(contact);
    }

    //Methods
    public boolean isEmpty(){
        return contacts.isEmpty();
    }
}
